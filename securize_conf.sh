#!/bin/sh

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^SMTP_PASSWORD=/c\SMTP_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^ADMINAPP_PASSWORD=/c\ADMINAPP_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^DDADMIN_PASSWORD=/c\DDADMIN_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^KEYCLOAK_PASSWORD=/c\KEYCLOAK_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^KEYCLOAK_DB_PASSWORD=/c\KEYCLOAK_DB_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^POSTGRES_PASSWORD=/c\POSTGRES_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^MARIADB_PASSWORD=/c\MARIADB_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^MOODLE_POSTGRES_PASSWORD=/c\MOODLE_POSTGRES_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^MOODLE_ADMIN_PASSWORD=/c\MOODLE_ADMIN_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^NEXTCLOUD_POSTGRES_PASSWORD=/c\NEXTCLOUD_POSTGRES_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^NEXTCLOUD_ADMIN_PASSWORD=/c\NEXTCLOUD_ADMIN_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^ETHERPAD_POSTGRES_PASSWORD=/c\ETHERPAD_POSTGRES_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^ETHERPAD_ADMIN_PASSWORD=/c\ETHERPAD_ADMIN_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^WORDPRESS_MARIADB_PASSWORD=/c\WORDPRESS_MARIADB_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^WORDPRESS_ADMIN_PASSWORD=/c\WORDPRESS_ADMIN_PASSWORD=$PWD" digitaldemocratic.conf

PWD=$(shuf -n3 /usr/share/dict/words | tr -d "\n" | tr -d "'")
sed -i "/^IPA_ADMIN_PWD=/c\IPA_ADMIN_PWD=$PWD" digitaldemocratic.conf
